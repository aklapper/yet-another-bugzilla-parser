#!/usr/bin/env php
<?php

// start from this bug ID
$start = 1;

// output filename
$DATA_FILE = 'data/bugzilla.json';

// whenever we should persist after every HTTP request
// when false, we persist only when the script is interrupted
$AUTOCOMMIT = false;

// no command line no party
if( !$argv ) {
	printf( "This is a command line script. ");
	exit(2);
}

// the first argument must be the base domain
$BASE_DOMAIN = $argv[1] ?? null;
if( !$BASE_DOMAIN ) {
	printf( "Usage: \n" );
	printf(
		"  %s https://bugzilla.example.com\n",
		$argv[0]
	);
	exit(2);
}

// avoid to end with a slash, since we will add that later
$BASE_DOMAIN = rtrim( $BASE_DOMAIN, '/' );

// Check whenever this is just a static dump. Since a static dump has probably a different format.
$IS_WMF_STATIC_DUMP = !filter_var($BASE_DOMAIN, FILTER_VALIDATE_URL);

// Canonical URL format of the activity page in Bugzilla.
// Plus, auto-guess a different page format if this Bugzilla was archived like here.
//    https://dumps.wikimedia.org/other/bugzilla/
$ACTIVITY_PAGE_FORMAT = "show_activity.cgi?id=%d";
if($IS_WMF_STATIC_DUMP) {
	$ACTIVITY_PAGE_FORMAT = "activity%d.html";
}

// Bugzilla has an history table with some cells
define('CELL_WHO',       0 );
define('CELL_WHEN',      1 );
define('CELL_WHAT',      2 );
define('CELL_REMOVED',   3 );
define('CELL_ADDED',     4 );
define('EXPECTED_CELLS', 5);

// Bugzilla may have some statuses that have no sense
define('NONSENSE_STATUSES', [
	'---',
	'ASSIGNED',
	'LATER',
	'NEW',
	'PATCH_TO_REVIEW',
	'REMIND',
	'UNCONFIRMED',
	'VERIFIED',
	'WORKSFORME',
	'CLOSED',
] );

define('ACTION_WITH_LINKS', [
	'Depends on',
	'Blocks',
] );

// Allow to shutdown without write issues
$GLOBALS['NOT_SHUTDOWN'] = true;
declare(ticks = 1);
pcntl_signal( SIGINT, 'shutdown' );

// collect some stats
$count_by_status = [];

// raw array with data
// this is the exact stuff that will be put as JSON
$data = null;

$to_be_saved = 0;

// while(1) :D
// note that the script is designed to be killed by WHATEVER error, including HTTP 404
// so this will never reach that scary number and will never cause a DOS
// we allow CTRL+C
for( $i = $start; $i < 999999999 && $GLOBALS['NOT_SHUTDOWN']; $i++ ) {

	if( $data === null ) {
		$data = read_data( $DATA_FILE );
	}

	// show progress
	echo "$i\n";

	// eventually parse again specific nonsense bugs.
	if( isset( $data['bugs'][$i]['lastStatus'] ) ) {
		if( is_nonsense_status( $data['bugs'][$i]['lastStatus'] ) ) {
			unset( $data['bugs'][$i] );
		}
	}

	// Parse a specific history page, if possible.
	$history_raw = null;
	if( !array_key_exists( $i, $data['bugs'] ) ) {
		try {
			// try to execute the HTTP request
			$history_raw = bugzilla_scrape_bug_history( $BASE_DOMAIN, $ACTIVITY_PAGE_FORMAT, $i );

		} catch( Exception $e ) {

			// mark this data as invalid
			$history_raw = false;

			// show what happening to the CLI console
			error_log( $e );

			// die soon
			$GLOBALS['NOT_SHUTDOWN'] = false;

			// force to save previous pending results.
			$AUTOCOMMIT = true;
		}

	}

	// parse the raw HTML history page and eventually update something
	$updated = false;
	if( $history_raw ) {

		$history = normalize_history( $history_raw );
		$status_history = filter_history_status( $history );
		if( $status_history ) {
			$last_status_change = end( $status_history );
			print_r($last_status_change);

			// persist last element
			$data['bugs'][$i]['lastStatus'] = $last_status_change[CELL_ADDED] ?? null;
			$data['bugs'][$i]['lastDate']   = $last_status_change[CELL_WHEN]  ?? null;
			$data['bugs'][$i]['lastActor']  = $last_status_change[CELL_WHO]   ?? null;
		} else {
			$data['bugs'][$i] = [];
		}

		print_r( $history );
		$data['bugs'][$i]['history'] = $history;

		$updated = true;
		$to_be_saved++;
	}

	// increment stats
	if( $history_raw !== false ) {
		$last_status = $data['bugs'][$i]['lastStatus'] ?? null;
		$count_by_status[ $last_status ] = $count_by_status[ $last_status ] ?? 0;
		$count_by_status[ $last_status ]++;
	}

	if( $updated ) {
		// During autocommit we write after any change but this can be a bottleneck.
		// So we also support batch writes.
		if( $AUTOCOMMIT || $to_be_saved > 50 ) {

			write_data( $DATA_FILE, $data );
			$to_be_saved = 0;

			// force another data read in the next loop
			$data = null;
		}
	}
}

// eventually write only if we changed something
if( $to_be_saved ) {
	write_data( $DATA_FILE, $data );
}

// show some nice stats
arsort( $count_by_status, SORT_NUMERIC );
print_r( $count_by_status );

// show a nice user message if you CTRL+C
if( !$GLOBALS['NOT_SHUTDOWN'] ) {
	echo "Exit by user input\n";
	exit(3);
}

/**
 * From history entries, take only ones related to status changes
 *
 * @return array
 */
function filter_history_status( array $history ) {

	$status_history = [];

	foreach( $history as $entry ) {
		// We are interested only in meaningful Status changes
		if( (
		      $entry[CELL_WHAT] === 'Status'
		      ||
		      $entry[CELL_WHAT] === 'Resolution'
		    )
		    &&
		    !is_nonsense_status( $entry[CELL_ADDED] )
		) {
			$status_history[] = $entry;
		}
	}

	return $status_history;
}

/**
 * Normalize a raw bug history
 *
 * @return array
 */
function normalize_history( array $history ) {

	$cleaned = [];

	foreach( $history as $entry ) {

		// in PHP, an array is never a pointer - this is a copy
		$copy = $entry;

		// we are not interested in link destination, just its text
		if( is_action_with_links( $copy[CELL_WHAT] ) ) {
			$copy[CELL_ADDED]   = strip_links( $copy[CELL_ADDED]   );
			$copy[CELL_REMOVED] = strip_links( $copy[CELL_REMOVED] );

		// there are weird nonsense attachment links in the "What" field
		} elseif( string_contains( $copy[CELL_WHAT], 'attachment.cgi' ) ) {
			$copy[CELL_WHAT] = strip_links( $copy[CELL_WHAT] );
		}

		$cleaned[] = $copy;
	}

	return $cleaned;
}

/**
 * Check whenever a status is known to be nonsense
 *
 * @return bool
 */
function is_nonsense_status( string $status ) {
	return in_array( $status, NONSENSE_STATUSES, true );
}

/**
 * Check whenever an action has some links
 *
 * @return bool
 */
function is_action_with_links( string $action ) {
	return in_array( $action, ACTION_WITH_LINKS, true );
}

/**
 * Get the history of a Bugzilla Bug ID
 */
function bugzilla_scrape_bug_history( string $base_domain, string $activity_page_format, int $bug_id ) {
	$url = $base_domain . '/' . sprintf($activity_page_format, $bug_id);
	$content = web_download($url);

	// parse history table
	$table_match = null;
	preg_match( '@<table[a-z0-9 ="]+>(.+)</table>@s', $content, $table_match );
	$table = $table_match[1] ?? null;
	if( !$table ) {

		$is_empty_for_a_reason =
			$content !== null &&
			string_contains( $content, 'No changes have been made to this bug yet' );

		if( !$is_empty_for_a_reason ) {
			throw new Exception( sprintf(
				"Cannot parse table from Bug %s",
				$url
			) );
		}
	}

	// parse history table rows
	$row_matches = [];
	$rows = preg_match_all( '@<tr>(.+?)</tr>@s', $table, $row_matches );
	if( $rows === false ) {
		throw new Exception( sprintf(
			"Cannot parse table rows from Bug %s",
			$url
		) );
	}

	$history = [];

	$last_columns = null;

	// for each row (skipping the heading)
	for( $row = 1; $row < $rows; $row++ ) {

		// for each table row, parse cells
		$row_content = $row_matches[ 1 ][ $row ];
		$cells = preg_match_all( '@<td[a-z0-9 ="]*>(.+?)</td>@s', $row_content, $cell_matches );
		if( $cells === false ) {
			throw new Exception( sprintf(
				"Cannot parse table cells from content: %s",
				$row_content
			) );
		}

		// Clean
		$columns = [];
		for( $cell = 0; $cell < $cells; $cell++ ) {
			$columns[ $cell ] = $cell_matches[ 1 ][ $cell ] ?? '';
			$columns[ $cell ] = purge_spaces( $columns[ $cell ] );
		}

		// Bugzilla omits the first cells if they are repeated...
		while( count( $columns ) < EXPECTED_CELLS ) {
			array_unshift( $columns, '' );
		}

		// Inherit omitted cells from the previous one
		if( $last_columns !== null ) {
			foreach( $columns as $i => $column ) {

				// If this column is already populated, also the following ones will be OK.
				if( $column !== '' ) {
					break;
				}

				$columns[$i] = $last_columns[$i] ?? '';
			}
		}


		$last_columns = $columns;

		$history[] = $columns;
	}

	return $history;
}

/**
 * Download a web page as raw HTML
 */
function web_download( string $url ) {
	$content = file_get_contents( $url );
	if(!$content) {
		throw new Exception( sprintf(
			"Cannot get contents from URL %s",
			$url
		) );
	}
	return $content;
}

/**
 * Read a file in JSON
 */
function read_data( string $file ) {
	$data = [];
	if( file_exists( $file ) ) {
		$content = file_get_contents( $file );
		$data = json_decode( $content, true, 512, JSON_THROW_ON_ERROR );
	}
	if( !isset( $data['bugs'] ) ) {
		$data['bugs'] = [];
	}
	return $data;
}

/**
 * Write a file in JSON
 */
function write_data( string $file, $data ) {

	echo "Saving...\n";

	ksort( $data['bugs'], SORT_NUMERIC );

	$content = json_encode( $data, JSON_PRETTY_PRINT );
	file_put_contents( $file, $content );
}

/**
 * Remove some unuseful spaces from a string
 *
 * @return string
 */
function purge_spaces( string $str ) {
	$str = str_replace( "\n", '', $str );
	$str = trim( $str );
	return $str;
}

/**
 * Check if a string contains something
 *
 * @return boolean
 */
function string_contains( string $haystack, string $needle ) {
	return mb_strpos( $haystack, $needle ) !== false;
}

/**
 * Strip all the links from a string and just get their text
 *
 * @return boolean
 */
function strip_links( string $links ) {
	$links = preg_replace( '@<a.+?>(.+?)</a>@s', '$1', $links );

	// these links contain an huge amount of internal spaces
	$links = preg_replace( '@  +@s', ' ', $links );

	$links = trim( $links );
	return $links;
}

/**
 * A stupid function that generates a shutdown
 */
function shutdown() {
	$GLOBALS['NOT_SHUTDOWN'] = false;
}
