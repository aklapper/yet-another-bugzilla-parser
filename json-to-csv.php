#!/usr/bin/env php
<?php
$start = 1;

// no command line no party
if( !$argv ) {
	echo "This is a command line script.";
	exit(2);
}

$in  = $argv[1] ?? null;
$out = $argv[2] ?? null;

// no first arguments, no party
if( !$in || !$out ) {
	printf( "Usage: \n" );
	printf( "  %s input.json output.csv\n", $argv[0] );
	exit(1);
}

// no file, no party
if( !file_exists( $in ) ) {
	printf( "File %s does not exist.\n", $in );
}

$content_in = file_get_contents( $in );
$content_data = json_decode( $content_in, true, 512, JSON_THROW_ON_ERROR );

$out_fp = fopen($out, 'w');
$header = [
	'BugID',
	'LastDateEpoch'
	'LastDate',
	'LastActor',
	'LastStatus',
];
fputcsv($out_fp, $header);

$bugs = $content_data['bugs'];

foreach( $bugs as $bug_id => $bug ) {
	if( isset( $bug['lastStatus'] ) ) {
		$row = [
			$bug_id,
			strtotime($bug['lastDate']),
			$bug['lastDate'],
			$bug['lastActor'],
			$bug['lastStatus'],
		];
		fputcsv($out_fp, $row);
	}
}

fclose($out_fp);
