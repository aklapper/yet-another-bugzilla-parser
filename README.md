# Yet Another Bugzilla Parser (in PHP)

This is a scraper for Bugzilla websites.

It was designed in 2023-07-09 to obtain the exact "close date" of all bugs.

For example, this simplifies migrations from Bugzilla to Phabricator/Phorge.

## Why you used PHP

Because PHP allowed to do this, in a dirty but readable way, without using any damn external library.

## Usage

From command line, just execute the main file:

```
./bugzilla-scraper.php https://bugzilla.example.com
```

It also supports a local dump (if you would like to download one first - like https://dumps.wikimedia.org/other/bugzilla/ )

```
./bugzilla-scraper.php /path/to/nice/static-bugzilla
```

It will write a JSON file inside the `data/` directory.

Then you can convert the JSON to a CSV with this utility:

```
./json-to-csv.php input.json output.csv
```

## Dataset

Please just look at the JSON file that is self-explaining.

Every data-block is indexed by the Bugzilla Bug ID.
